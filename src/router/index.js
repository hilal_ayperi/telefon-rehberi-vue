import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

// import Anasayfa from '../views/Anasayfa.vue'
import firebase from "firebase/app"
import store from '../store/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    // meta:{
    //   requiresAuth: true
    // }
  },
  {
    path: '/kisiekle',
    name: 'Kişi Ekle',
    component: () => import('../views/KisiEkle.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/guncelle/:tc',
    name: 'Güncelle',
    component: () => import('../views/Guncelle.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/anasayfa',
    name: 'Anasayfa',
    component: () => import('../views/Anasayfa.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/hakkımızda',
    name: 'Hakkımızda',
    component: () => import('../views/Hakkımızda.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/iletisim',
    name: 'iletisim',
    component: () => import('../views/iletisim.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/cikis',
    name: 'Cikis',
    component: () => import('../views/Cikis.vue')
  },
  {
    path: '/kayit',
    name: 'Kayit',
    component: () => import('../views/Kayit.vue')
  },
  {
    path: '/sifre',
    name: 'sifre',
    component: () => import('../views/sifre.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/bilgiler',
    name: 'bilgiler',
    component: () => import('../views/bilgiler.vue'),
    meta:{
      requiresAuth: true
    }
  },
  {
    path: '/dogrulama',
    name: 'dogrulama',
    component: () => import('../views/dogrulama.vue'),
    
  }
]
const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

router.beforeEach((to,from,next)=>{
  const currentUser = firebase.auth().currentUser; //yeni kullanıcı varsa
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
if(to.matched.some(record => record.meta.requiresAuth)){

  firebase.auth().onAuthStateChanged(function(user) {

    if (user) {

      next();
      return user
    } else {
      next('Login')
      return user
    }
  });

}
else{
  next();
}
  
})
export default router


import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueSimpleAlert from "vue-simple-alert"
import { store } from "./store/store";
import Vuelidate from 'vuelidate';
import UniqueId from 'vue-unique-id';
import Pagination from 'vue-pagination-2';
import VueResource from 'vue-resource'
import firebase from "firebase/app";
import "firebase/auth"
import toastr from "toastr"
import {BootstrapVue,IconsPlugin} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import StackModal from '@innologica/vue-stackable-modal'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import VueSweetalert2 from 'vue-sweetalert2';
 
import 'sweetalert2/dist/sweetalert2.min.css';

import i18n from './plugins/i18n'

Vue.config.productionTip = false


const options = {
};


Vue.use(Toast, options);


Vue.use(StackModal)

Vue.use(VueResource)

Vue.use(BootstrapVue)

Vue.use(IconsPlugin)

Vue.config.productionTip = false

Vue.use(Vuelidate)

Vue.use(VueSimpleAlert );

Vue.use(UniqueId);

Vue.component('pagination',Pagination);

Vue.http.options.root = "https://telefon-rehber.firebaseio.com/products";

Vue.config.productionTip = false;

let config = {
  apiKey: 'AIzaSyA1qCnIPs4xBMXNpxi99T_ZL4_RTw8yab8',
  authDomain: 'telefon-rehber.firebaseapp.com',
  databaseURL: 'https://telefon-rehber.firebaseio.com',
  projectId: 'telefon-rehber',
  storageBucket: 'gs://telefon-rehber.appspot.com',
  messagingSenderId: '276462767962',
  appId: '1:276462767962:web:18a190086bb4d6d68dad63',
  measurementId: 'G-7NT3RDPP3M'
};

firebase.initializeApp(config);

new Vue({
  el:"#app",
  router,
  render: h => h(App),
  store,
  i18n,
  components: { App },
  template: '<App/>'

}).$mount('#app')

